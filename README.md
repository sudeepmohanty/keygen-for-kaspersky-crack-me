# This script generates a valid serial key for the Kaspersky Crack Me program

# -------- Conditions for a valid key -------- #
# 1. Length of the serial key should be = 19 (decimal)
# 2. The serial key should contain 3 hyphens (-)
# 3. Every number in the serial key should be less than 9 (decimal)
# 4. The key should be of the form XXXX-XXXX-XXXX-XXXX
# 5. The first block in the key should add up to 202 (decimal - ascii vals)
# 6. The second block of the key should add up to 201 (decimal - ascii vals)
# 7. The third block in the key should add up to 200 (decimal - ascii vals)
# 8. The fourth block of the key should add up to 199 (decimal - ascii vals)
# 9. The sum of all the numbers in the key should add up to (203 x 4 - ascii vals) decimal
# 10. None of the numbers in the fourth block should be equal to the respective numbers of the first or second block
# 11. None of the numbers in the second block should be equal to the respective numbers of the third block
