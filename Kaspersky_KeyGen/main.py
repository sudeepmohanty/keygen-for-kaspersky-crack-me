__author__ = 'Sudeep Mohanty'
#####################################################################################################################
# This is my try at learning to code in Python
# This script generates a valid serial key for the Kaspersky Crack Me program

# -------- Conditions for a valid key -------- #
# 1. Length of the serial key should be = 19 (decimal)
# 2. The serial key should contain 3 hyphens (-)
# 3. Every number in the serial key should be less than 9 (decimal)
# 4. The key should be of the form XXXX-XXXX-XXXX-XXXX
# 5. The first block in the key should add up to 202 (decimal - ascii vals)
# 6. The second block of the key should add up to 201 (decimal - ascii vals)
# 7. The third block in the key should add up to 200 (decimal - ascii vals)
# 8. The fourth block of the key should add up to 199 (decimal - ascii vals)
# 9. The sum of all the numbers in the key should add up to (203 x 4 - ascii vals) decimal
# 10. None of the numbers in the fourth block should be equal to the respective numbers of the first or second block
# 11. None of the numbers in the second block should be equal to the respective numbers of the third block
#####################################################################################################################

import random
import tkinter

serialKey = 'XXXX-XXXX-XXXX-XXXX'

def keygen():

    # Declare each block of 4 numbers as lists
    block1 = list()
    block2 = list()
    block3 = list()
    block4 = list()

    # Number of Blocks
    blockNumber = 4

    while blockNumber != 0:
        # The numbers in each block
        number1 = 0
        number2 = 0
        number3 = 0
        number4 = 0

        # Generate Block 4
        if blockNumber == 4:
            while (ord(str(number1)) + ord(str(number2)) + ord(str(number3)) + ord(str(number4))) != (203 - blockNumber):
                number1 = random.randint(0, 8)
                number2 = random.randint(0, 8)
                number3 = random.randint(0, 8)
                number4 = random.randint(0, 8)
            block4.insert(0, number1)
            block4.insert(1, number2)
            block4.insert(2, number3)
            block4.insert(3, number4)

        # Generate Block 3
        elif blockNumber == 3:
            while (ord(str(number1)) + ord(str(number2)) + ord(str(number3)) + ord(str(number4))) != (203 - blockNumber):
                number1 = random.randint(0, 8)
                number2 = random.randint(0, 8)
                number3 = random.randint(0, 8)
                number4 = random.randint(0, 8)
            block3.insert(0, number1)
            block3.insert(1, number2)
            block3.insert(2, number3)
            block3.insert(3, number4)

        # Generate Block 2
        elif blockNumber == 2:
            while (ord(str(number1)) + ord(str(number2)) + ord(str(number3)) + ord(str(number4))) != (203 - blockNumber):
                number1 = random.randint(0, 8)
                number2 = random.randint(0, 8)
                number3 = random.randint(0, 8)
                number4 = random.randint(0, 8)
                if number1 == block4[0] or number1 == block3[0] \
                   or number2 == block4[1] or number1 == block3[1] \
                   or number3 == block4[2] or number3 == block3[2] \
                   or number4 == block4[3] or number4 == block3[3]:
                        number1 = 0
                        number2 = 0
                        number3 = 0
                        number4 = 0
            block2.insert(0, number1)
            block2.insert(1, number2)
            block2.insert(2, number3)
            block2.insert(3, number4)

        # Generate Block 1
        else:
            while (ord(str(number1)) + ord(str(number2)) + ord(str(number3)) + ord(str(number4))) != (203 - blockNumber):
                number1 = random.randint(0, 8)
                number2 = random.randint(0, 8)
                number3 = random.randint(0, 8)
                number4 = random.randint(0, 8)
                if number1 == block4[0] \
                   or number2 == block4[1] \
                   or number3 == block4[2] \
                   or number4 == block4[3]:
                        number1 = 0
                        number2 = 0
                        number3 = 0
                        number4 = 0
            block1.insert(0, number1)
            block1.insert(1, number2)
            block1.insert(2, number3)
            block1.insert(3, number4)

        blockNumber -= 1

    # Add all the blocks with hyphens to generate the serial key
    global serialKey
    serialKey = str(block1[0]) + str(block1[1]) + str(block1[2]) + str(block1[3]) + '-' + \
                str(block2[0]) + str(block2[1]) + str(block2[2]) + str(block2[3]) + '-' + \
                str(block3[0]) + str(block3[1]) + str(block3[2]) + str(block3[3]) + '-' + \
                str(block4[0]) + str(block4[1]) + str(block4[2]) + str(block4[3])

    #print('Serial Key: ', serialKey)
    key = tkinter.Text(window, height=1, borderwidth=0)
    key.insert(1.0, serialKey)
    key.pack()
    key.update()

window = tkinter.Tk()
window.title('KeyGen for Kaspersky Crack Me')
window.geometry('400x500')
generateBtn = tkinter.Button(window, text="Generate", command=keygen)
generateBtn.pack()
window.mainloop()
